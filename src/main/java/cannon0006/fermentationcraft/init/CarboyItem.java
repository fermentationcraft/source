package cannon0006.fermentationcraft.init;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CarboyItem extends Item
{
	public CarboyItem(String name)
	{
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(CreativeTabs.BREWING);
	}
}
