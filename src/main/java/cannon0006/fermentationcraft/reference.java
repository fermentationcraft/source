package cannon0006.fermentationcraft;

public class Reference
{
	public static final String MODID = "fc";
	public static final String NAME = "FermentationCraft";
	public static final String VERSION = "1.0-Alpha";
	
	public static final String CLIENTPROXY = "cannon0006.fermentationcraft.proxy.ClientProxy";
	public static final String COMMONPROXY = "cannon0006.fermentationcraft.proxy.CommonProxy";
}
