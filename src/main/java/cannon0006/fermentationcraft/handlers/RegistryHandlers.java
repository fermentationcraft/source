package cannon0006.fermentationcraft.handlers;

import cannon0006.fermentationcraft.init.ItemInit;

public class RegistryHandlers
{
	public static void Client()
	{
		ItemInit.register();
	}
	
	public static void Common()
	{
		ItemInit.init();
	}
}
